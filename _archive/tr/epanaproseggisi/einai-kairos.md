---
layout: post
title: 'Είναι καιρός'
categories:
  - 'epanaproseggisi'
tags:
  - 'Επαναπροσέγγιση'
pdf_path: './assets/archive/tr/epanaproseggisi/einai-kairos.pdf'
pdf_name: '2000 - Είναι καιρός - TR.pdf'
year: '2000'
language: 'Τουρκική'
---
