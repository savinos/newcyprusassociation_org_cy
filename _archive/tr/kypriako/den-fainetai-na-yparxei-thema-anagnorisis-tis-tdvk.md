---
layout: post
title: 'Δεν φαίνεται να υπάρχει θέμα αναγνώρισης της ΤΔΒΚ'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/tr/kypriako/den-fainetai-na-yparxei-thema-anagnorisis-tis-tdvk.pdf'
pdf_name: '2004 - Δεν φαίνεται να υπάρχει θέμα αναγνώρισης της ΤΔΒΚ - TR.pdf'
year: '2004'
language: 'Τουρκική'
---
