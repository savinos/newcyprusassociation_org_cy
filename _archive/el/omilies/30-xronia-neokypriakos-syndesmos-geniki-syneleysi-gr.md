---
layout: post
title: '30 χρόνια Νεοκυπριακός Σύνδεσμος Γενική Συνέλευση GR'
categories:
  - 'omilies'
tags:
  - 'Ομιλίες'
pdf_path: './assets/archive/el/omilies/30-xronia-neokypriakos-syndesmos-geniki-syneleysi-gr.pdf'
pdf_name: '2005 - 30 χρόνια Νεοκυπριακός Σύνδεσμος - Γενική Συνέλευση - GR.pdf'
year: '2005'
language: 'Ελληνικά'
---
