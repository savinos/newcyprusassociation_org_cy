---
layout: post
title: '25 χρόνια από την ιδρυση του ΝΚΣ GR'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/el/peri-syndesmoy/25-xronia-apo-tin-idrysi-toy-nks-gr.pdf'
pdf_name: '25 χρόνια από την ιδρυση του ΝΚΣ - GR - 2000.pdf'
year: '2000'
language: 'Ελληνικά'
---
