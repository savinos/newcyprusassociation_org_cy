---
layout: post
title: '40 χρόνια Κυπριακής Δημοκρατίας'
categories:
  - 'omilies'
tags:
  - 'Ομιλίες'
pdf_path: './assets/archive/omilies/40-xronia-kypriakis-dimokratias.pdf'
pdf_name: '2000 - 40 χρόνια Κυπριακής Δημοκρατίας.pdf'
year: '2000'
---
