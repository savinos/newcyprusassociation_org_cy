---
layout: post
title: 'Υπέρβαση Κοινοτισμού Προβλήματα και Προπτικές'
categories:
  - 'omilies'
tags:
  - 'Ομιλίες'
pdf_path: './assets/archive/omilies/ypervasi-koinotismoy-provlimata-kai-prooptikes.pdf'
pdf_name: '2011 - Υπέρβαση Κοινοτισμού - Προβλήματα και Προοπτικές.pdf'
year: '2011'
---
