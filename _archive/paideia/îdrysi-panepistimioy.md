---
layout: post
title: 'Ίδρυση Πανεπιστημίου'
categories:
  - 'paideia'
tags:
  - 'Παιδεία'
pdf_path: './assets/archive/paideia/îdrysi-panepistimioy.pdf'
pdf_name: '1985 - Ίδρυση Πανεπιστημίου.pdf'
year: '1985'
---
