---
layout: post
title: 'Υπόμνημα για τον Υπουργό Παιδείας'
categories:
  - 'paideia'
tags:
  - 'Παιδεία'
pdf_path: './assets/archive/paideia/ypomnima-gia-ton-ypoyrgo-paideias.pdf'
pdf_name: '1999 - Υπόμνημα για τον Υπουργό Παιδείας.pdf'
year: '1999'
---
