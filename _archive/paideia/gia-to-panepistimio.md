---
layout: post
title: 'Για το Πανεπιστήμιο'
categories:
  - 'paideia'
tags:
  - 'Παιδεία'
pdf_path: './assets/archive/paideia/gia-to-panepistimio.pdf'
pdf_name: '1997 - Για το Πανεπιστήμιο.pdf'
year: '1997'
---
