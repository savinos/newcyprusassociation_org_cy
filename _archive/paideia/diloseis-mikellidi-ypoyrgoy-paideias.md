---
layout: post
title: 'Δηλώσεις Μικελίδη (Υπουργού Παιδείας)'
categories:
  - 'paideia'
tags:
  - 'Παιδεία'
pdf_path: './assets/archive/paideia/diloseis-mikellidi-ypoyrgoy-paideias.pdf'
pdf_name: '1975 - Δηλώσεις Μικελλίδη (Υπουργού Παιδείας).pdf'
year: '1975'
---
