---
layout: post
title: 'Επιστολή Αρχιεπισκόπου προς ΚΙΜΑ3'
categories:
  - 'ekklisia'
tags:
  - 'Εκκλησία'
pdf_path: './assets/archive/ekklisia/epistoli-arxiepiskopoy-pros-kima3.pdf'
pdf_name: '1983 - Επιστολή Αρχιεπισκόπου προς ΚΙΜΑ-3.pdf'
year: '1983'
---
