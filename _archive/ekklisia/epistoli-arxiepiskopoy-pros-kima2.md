---
layout: post
title: 'Επιστολή Αρχιεπισκόπου προς ΚΙΜΑ2'
categories:
  - 'ekklisia'
tags:
  - 'Εκκλησία'
pdf_path: './assets/archive/ekklisia/epistoli-arxiepiskopoy-pros-kima2.pdf'
pdf_name: '1983 - Επιστολή Αρχιεπισκόπου προς ΚΙΜΑ-2.pdf'
year: '1983'
---
