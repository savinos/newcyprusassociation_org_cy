---
layout: post
title: 'Επιστολή Αρχιεπισκόπου προς ΚΙΜΑ1'
categories:
  - 'ekklisia'
tags:
  - 'Εκκλησία'
pdf_path: './assets/archive/ekklisia/epistoli-arxiepiskopoy-pros-kima1.pdf'
pdf_name: '1983 - Επιστολή Αρχιεπισκόπου προς ΚΙΜΑ-1.pdf'
year: '1983'
---
