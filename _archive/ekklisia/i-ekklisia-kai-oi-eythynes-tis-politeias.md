---
layout: post
title: 'Η Εκλησία και οι ευθύνες της Πολιτείας'
categories:
  - 'ekklisia'
tags:
  - 'Εκκλησία'
pdf_path: './assets/archive/ekklisia/i-ekklisia-kai-oi-eythynes-tis-politeias.pdf'
pdf_name: '1995 - Η Εκκλησία και οι ευθύνες της Πολιτείας.pdf'
year: '1995'
---
