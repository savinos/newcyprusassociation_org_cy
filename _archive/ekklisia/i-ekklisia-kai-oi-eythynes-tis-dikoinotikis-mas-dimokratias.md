---
layout: post
title: 'Η Εκλησία και οι ευθύνες της Δικοινοτικής μας Δημοκρατίας'
categories:
  - 'ekklisia'
tags:
  - 'Εκκλησία'
pdf_path: './assets/archive/ekklisia/i-ekklisia-kai-oi-eythynes-tis-dikoinotikis-mas-dimokratias.pdf'
pdf_name: '2006 - Η Εκκλησία και οι ευθύνες της Δικοινοτικής μας Δημοκρατίας.pdf'
year: '2006'
---
