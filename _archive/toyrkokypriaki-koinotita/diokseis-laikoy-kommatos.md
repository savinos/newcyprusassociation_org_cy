---
layout: post
title: 'Διώξεις Λαϊκού Κόματος'
categories:
  - 'toyrkokypriaki-koinotita'
tags:
  - 'Τουρκοκυπριακή Κοινότητα'
pdf_path: './assets/archive/toyrkokypriaki-koinotita/diokseis-laikoy-kommatos.pdf'
pdf_name: '1975 - Διώξεις Λαϊκού Κόμματος.pdf'
year: '1975'
---
