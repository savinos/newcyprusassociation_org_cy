---
layout: post
title: 'Την Εξωτερική Πολιτική θα πρέπει να διέπει πραγματισμός'
categories:
  - 'diethni'
tags:
  - 'Διεθνή'
pdf_path: './assets/archive/diethni/tin-eksoteriki-politiki-tha-prepei-na-diepei-pragmatismos.pdf'
pdf_name: '1994 - Την Εξωτερική Πολιτική θα πρέπει να διέπει πραγματισμός.pdf'
year: '1994'
---
