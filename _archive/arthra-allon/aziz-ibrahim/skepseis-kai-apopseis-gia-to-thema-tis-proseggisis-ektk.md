---
layout: post
title: 'Σκέψεις και απόψεις για το θέμα της προσέγισης ΕΚΤΚ'
categories:
  - 'arthra-allon'
  - 'aziz-ibrahim'
tags:
  - 'Άρθρα Άλλων'
  - 'Aziz Ibrahim'
pdf_path: './assets/archive/arthra-allon/aziz-ibrahim/skepseis-kai-apopseis-gia-to-thema-tis-proseggisis-ektk.pdf'
pdf_name: 'Σκέψεις και απόψεις για το θέμα της προσέγγισης ΕΚ-ΤΚ.pdf'
---
