---
layout: post
title: 'Spech at the Lions Club of Nicosia'
categories:
  - 'omilies'
tags:
  - 'Ομιλίες'
pdf_path: './assets/archive/en/omilies/speech-at-the-lions-club-of-nicosia.pdf'
pdf_name: '2003 - Speech at the Lions Club of Nicosia - EN.pdf'
year: '2003'
language: 'Αγγλικά'
---
