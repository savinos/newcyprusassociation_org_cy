---
layout: post
title: 'Ιχσάν Αλή (The unforgoten leader Dr. Ihsan Ali)'
categories:
  - 'kyprioi-agonistes'
tags:
  - 'Κύπριοι Αγωνιστές'
pdf_path: './assets/archive/en/kyprioi-agonistes/ixsan-ali-the-unforgotten-leader-dr-ihsan-ali.pdf'
pdf_name: 'Ιχσάν Αλή - 2005 - EN (The unforgotten leader Dr. Ihsan Ali).pdf'
year: '2005'
language: 'Αγγλικά'
---
