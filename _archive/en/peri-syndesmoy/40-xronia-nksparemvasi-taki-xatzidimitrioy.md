---
layout: post
title: '40 Χρόνια ΝΚΣΠαρέμβαση Τάκη Χατζηδημητρίου'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/en/peri-syndesmoy/40-xronia-nksparemvasi-taki-xatzidimitrioy.pdf'
pdf_name: '40 Χρόνια ΝΚΣ-Παρέμβαση Τάκη Χατζηδημητρίου-EN.pdf'
language: 'Αγγλικά'
---
