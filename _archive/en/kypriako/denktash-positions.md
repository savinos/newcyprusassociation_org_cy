---
layout: post
title: 'Denktash positions'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/en/kypriako/denktash-positions.pdf'
pdf_name: '1976 - Denktash positions.pdf'
year: '1976'
language: 'Αγγλικά'
---
