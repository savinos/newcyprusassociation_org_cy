---
layout: post
title: 'Ψήφιση Νόμου για Ενωτικό Δημοψήφισμα'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/psifisi-nomoy-gia-enotiko-dimopsifisma.pdf'
pdf_name: '2017 - Ψήφιση Νόμου για Ενωτικό Δημοψήφισμα.pdf'
year: '2017'
---
