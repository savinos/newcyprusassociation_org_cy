---
layout: post
title: 'Παράδοση ΤΚ στον Ντεκτάς'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/paradosi-tk-ston-ntektas.pdf'
pdf_name: '1984 - Παράδοση ΤΚ στον Ντεκτάς.pdf'
year: '1984'
---
