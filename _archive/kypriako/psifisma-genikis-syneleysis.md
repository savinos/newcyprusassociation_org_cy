---
layout: post
title: 'Ψήφισμα Γενικής Συνέλευσης'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/psifisma-genikis-syneleysis.pdf'
pdf_name: '2017 - Ψήφισμα Γενικής Συνέλευσης.pdf'
year: '2017'
---
