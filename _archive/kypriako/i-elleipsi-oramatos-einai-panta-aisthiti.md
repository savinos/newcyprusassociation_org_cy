---
layout: post
title: 'Η έλειψη οράματος είναι πάντα αισθητή'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/i-elleipsi-oramatos-einai-panta-aisthiti.pdf'
pdf_name: '1999 - Η έλλειψη οράματος είναι πάντα αισθητή.pdf'
year: '1999'
---
