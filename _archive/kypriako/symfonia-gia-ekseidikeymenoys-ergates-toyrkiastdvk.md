---
layout: post
title: 'Συμφωνία για εξειδικευμένους εργάτες ΤουρκίαςΤΔΒΚ'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/symfonia-gia-ekseidikeymenoys-ergates-toyrkiastdvk.pdf'
pdf_name: '1987 - Συμφωνία για εξειδικευμένους εργάτες Τουρκίας-ΤΔΒΚ.pdf'
year: '1987'
---
