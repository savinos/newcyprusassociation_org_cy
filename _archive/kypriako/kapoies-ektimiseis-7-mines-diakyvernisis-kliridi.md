---
layout: post
title: 'Κάποιες Εκτιμήσεις (7 μήνες διακυβέρνησης Κληρίδη)'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/kapoies-ektimiseis-7-mines-diakyvernisis-kliridi.pdf'
pdf_name: '1993 - Κάποιες Εκτιμήσεις (7 μήνες διακυβέρνησης Κληρίδη).pdf'
year: '1993'
---
