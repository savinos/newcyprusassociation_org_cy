---
layout: post
title: 'Πρωτοβουλία Σοβιετικής Ένωσης'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/protovoylia-sovietikis-enosis.pdf'
pdf_name: '1986 - Πρωτοβουλία Σοβιετικής Ένωσης.pdf'
year: '1986'
---
