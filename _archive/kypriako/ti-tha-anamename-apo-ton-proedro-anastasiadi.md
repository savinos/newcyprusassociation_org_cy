---
layout: post
title: 'Τι θα αναμέναμε από τον Πρόεδρο Αναστασιάδη'
categories:
  - 'kypriako'
tags:
  - 'Κυπριακό'
pdf_path: './assets/archive/kypriako/ti-tha-anamename-apo-ton-proedro-anastasiadi.pdf'
pdf_name: '2017 - Τι θα αναμέναμε από τον Πρόεδρο Αναστασιάδη.pdf'
year: '2017'
---
