---
layout: post
title: 'Ανακήρυξη ΤΔΒΚ (3 χρόνια)'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/anakiryksi-tdvk-3-xronia.pdf'
pdf_name: 'Ανακήρυξη ΤΔΒΚ (3 χρόνια) - 1986.pdf'
year: '1986'
---
