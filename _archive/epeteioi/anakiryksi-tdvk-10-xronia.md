---
layout: post
title: 'Ανακήρυξη ΤΔΒΚ (10 χρόνια)'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/anakiryksi-tdvk-10-xronia.pdf'
pdf_name: 'Ανακήρυξη ΤΔΒΚ (10 χρόνια) - 1993.pdf'
year: '1993'
---
