---
layout: post
title: 'Ανακήρυξη ΤΔΒΚ (31 χρόνια) (Πολίτης)'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/anakiryksi-tdvk-31-xronia-politis.pdf'
pdf_name: 'Ανακήρυξη ΤΔΒΚ (31 χρόνια) - 2014 (Πολίτης).pdf'
year: '2014'
---
