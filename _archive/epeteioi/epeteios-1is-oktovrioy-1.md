---
layout: post
title: 'Επέτειος 1ης Οκτωβρίου 1'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/epeteios-1is-oktovrioy-1.pdf'
pdf_name: 'Επέτειος 1ης Οκτωβρίου - 1979-1.pdf'
year: '1979'
---
