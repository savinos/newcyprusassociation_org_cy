---
layout: post
title: 'Ανακήρυξη ΤΔΒΚ (1 χρόνος)'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/anakiryksi-tdvk-1-xronos.pdf'
pdf_name: 'Ανακήρυξη ΤΔΒΚ (1 χρόνος) - 1984.pdf'
year: '1984'
---
