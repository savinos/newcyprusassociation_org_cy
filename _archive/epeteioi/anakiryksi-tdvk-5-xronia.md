---
layout: post
title: 'Ανακήρυξη ΤΔΒΚ (5 χρόνια)'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/anakiryksi-tdvk-5-xronia.pdf'
pdf_name: 'Ανακήρυξη ΤΔΒΚ (5 χρόνια) - 1988.pdf'
year: '1988'
---
