---
layout: post
title: 'Επέτειος 1ης Οκτωβρίου'
categories:
  - 'epeteioi'
tags:
  - 'Επέτειοι'
pdf_path: './assets/archive/epeteioi/epeteios-1is-oktovrioy.pdf'
pdf_name: 'Επέτειος 1ης Οκτωβρίου - 1987.pdf'
year: '1987'
---
