---
layout: post
title: 'ΝΚΣ Ιδρυτικό Έγραφο του ΝΚΣ (Προσχέδιο)'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/peri-syndesmoy/nks-idrytiko-eggrafo-toy-nks-prosxedio.pdf'
pdf_name: 'ΝΚΣ - Ιδρυτικό Έγγραφο του ΝΚΣ - 1974 (Προσχέδιο).pdf'
year: '1974'
---
