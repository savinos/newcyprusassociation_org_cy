---
layout: post
title: 'Παρουσίαση ΝΚΣ στη Λεμεσό'
categories:
  - 'peri-syndesmoy'
tags:
  - 'Περί Συνδέσμου'
pdf_path: './assets/archive/peri-syndesmoy/paroysiasi-nks-sti-lemeso.pdf'
pdf_name: '1978 - Παρουσίαση ΝΚΣ στη Λεμεσό.pdf'
year: '1978'
---
