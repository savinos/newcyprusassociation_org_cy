# class for collection
# class per file
require 'fileutils'
require "babosa"

LANGUAGES = ['EN', 'TR', 'GR', 'ΕΝ']
ARCHIVE_PATH_DEPTH = 4
TURKISH_CHARS = ['Ç', 'Ğ', 'İ', 'Ö', 'Ş', 'Ü', 'ç', 'ğ', 'ö', 'ş', 'ü']
EXTENSION_LENGTH = 5
REMOVE_FROM_FILENAME = ['.pdf', '&', '-']
LANGUAGE_IN_GREEK = {"EN" => "Αγγλικά", "TR" => "Τουρκική", "EL" => "Ελληνικά"}
# @path 
# @language = nil
# @year = nil
# @categories = []
# @categories_slugified = []
# @title_slugifed = nil-

def contains_turkish_chars? string
  TURKISH_CHARS.each do |char|
    if string.include?(char)
      return true
    end
  end
  return false
end

def guess_language string
  if contains_turkish_chars? string
    return 'tr'
  elsif /\p{Greek}/.match(string) != nil
    return 'el'
  else
    return 'en'
  end
end

# get language
# TODO check from const
# TODO exclude cases that are inside the word - how?
def get_language path
  # LANGUAGES.each do |lang| 
  # end
  language = path['EN'] || path['TR'] || path['GR'] || path['ΕΝ']
  if language == 'GR'
    language = 'EL'
  end
  language
end

# get year
def get_year path
  year_match_idx = path =~ /\d{4}/
  year = nil
  if year_match_idx != nil
    year = path[year_match_idx..year_match_idx + 3]
  end
  year
end

def delete_language_tag string
  # TODO with string or array
end

# get categories as are (for keywords in metadata and description)
def get_categories path
  categories = path.split('/')[ARCHIVE_PATH_DEPTH-1..-2]
  deleted = categories.delete_if{ |c| LANGUAGES.include?(c) }
  categories
end

def get_slugified(string)
  lang = guess_language string
  slugified = ''
  if lang == 'el'
    slugified = string.to_slug.normalize(transliterations: :greek).to_s
  else
    slugified = string.to_slug.normalize.to_s
  end
  slugified
end

# get categories in slugified
def get_categories_slugified path
  categories_slugified = []
  categories_array = get_categories path
  categories_array.each do |category|
    categories_slugified << get_slugified(category)
  end
  categories_slugified
end

def get_simplified_title(title, exclude_words)
  word_to_exclude_from_title = exclude_words + REMOVE_FROM_FILENAME
  word_to_exclude_from_title.each do |word|
    title = title.gsub word, ''
  end
  return title
end

# get the slugified filename for url (without language + pdf)
# TODO if year and language are not find them
def get_filename_url(path, exclude_words)
  file_name = path.split('/')[-1]
  title = get_simplified_title(file_name, exclude_words)
  slugified_file_name = get_slugified(title)
  return slugified_file_name
end

# get file name (without pdf)
def get_file_name_nl(path, exclude_words)
  file_name = path.split('/')[-1]
  title = get_simplified_title(file_name, exclude_words)
  title = title.squeeze.lstrip.rstrip
  return title
end

# get the new file name
def get_new_filename path
end

def get_path(language, categories_slugified)
  path = ''
  if language
    path = language.downcase + '/'
  end
  path = path + categories_slugified.join('/') + '/'
end

def copy_new_file(path, language, categories_slugified, title_slugified)
  new_dir = './assets/archive/' + get_path(language, categories_slugified)
  FileUtils.mkdir_p new_dir
  new_file = new_dir + title_slugified + '.pdf'
  FileUtils.cp path, new_file, :verbose => true
  return new_file
end

# get seo description (Turkish + English + Greek according to language)
def get_seo_description
end

def simplified(string)
  new_string = string.gsub '\'', ''
  return new_string
end

def walk(start)
  Dir.foreach(start) do |file_name|
    path = File.join(start, file_name)
    if file_name == "." or file_name == ".."
      next
    elsif File.directory?(path)
      # puts path + "/" # remove this line if you want; just prints directories
      walk(path)
    else
      puts path
      language = get_language path
      if language
        puts "Language: " + language
      end
      year = get_year path
      if year
        puts "Year: " + year
      end
      categories = get_categories path
      if !categories.empty?
        puts "Categories: " + categories.join(', ')
      end
      categories_slugified = get_categories_slugified path
      if !categories_slugified.empty?
        puts "Categories slugified: " + categories_slugified.join(', ')
      end
      exclude_words = []
      if year
        exclude_words << year
      end
      if language
        exclude_words << language
      end
      title_slugified = get_filename_url(path, exclude_words)
      puts "Title Slugified: " + title_slugified
      nl_title = get_file_name_nl(path, exclude_words)
      puts "Natural Language Title: " + nl_title
      # copy file to new
      file_path = copy_new_file(path, language, categories_slugified, title_slugified)
      
      # create directory for archive category
      article_path = '_archive/' + get_path(language, categories_slugified)
      FileUtils.mkdir_p article_path
      File.open(File.expand_path(article_path + title_slugified + '.md'), 'w') { |file|
        file.write("---\n")
        file.write("layout: post\n")
        file.write("title: '#{simplified(nl_title)}'\n")
        file.write("categories:\n")
        categories_slugified.each do |cat|
          file.write("  - '#{cat}'\n")
        end
        file.write("tags:\n")
        categories.each do |cat|
          file.write("  - '#{cat}'\n")
        end
        file.write("pdf_path: '#{file_path}'\n")
        pdf_name = simplified(file_name)
        file.write("pdf_name: '#{pdf_name}'\n")
        if year
          file.write("year: '#{year}'\n")
        end
        if language
          lang_in_greek = LANGUAGE_IN_GREEK[language]
          file.write("language: '#{lang_in_greek}'\n")
        end
        file.write("---\n")
      }
      puts
    end
  end
end

walk('./notes/NCA-Archive/')
