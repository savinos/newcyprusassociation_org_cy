---
layout: page
title: Contact Us
permalink: /contact/
---

New Cyprus Association

P.O.Box {{ site.pobox }}

{% assign locality = site.address.locality | split: "," %}
CY-{{ site.address.postcode }} {{ locality.first }} 

<a href="mailto:{{ site.email }}">{{ site.email }}</a>

<a href="tel:{{ site.telephone }}">{{ site.telephone }}</a>
